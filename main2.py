import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from pygame import *
import math
import resource
import random
import os.path
import shelve

if os.path.exists("data/save"):
    ulozeni = shelve.open('data/save')
    WIN_WIDTH = ulozeni['WIN_WIDTH']
    WIN_HEIGHT = ulozeni['WIN_HEIGHT']
    velikost = ulozeni['velikost']
    ulozeni.close()
else:
    ulozeni = shelve.open('data/save')
    WIN_WIDTH = 1024
    WIN_HEIGHT = 600
    velikost = 32
    ulozeni['WIN_WIDTH'] = WIN_WIDTH
    ulozeni['WIN_HEIGHT'] = WIN_HEIGHT
    ulozeni['velikost'] = velikost
    ulozeni.close()
#cerna = (0, 0, 0)
#bila = (255, 255, 255)
#cervena = (255, 0, 0)
#zelena = (0, 255, 0)
#modra = (0, 0, 255)
#DEPTH = 0
#FLAGS = 0
#resource.set_images_path("data")

#levels = {0: {'level': levels.level0()}}

game_time= None
player_model = None
other_model = None

WIDTH = 10
HEIGHT = 8
MAP =[[0,0,0,0,0,0,0,0,0,0,0],
      [0,1,1,1,1,1,1,1,0,1,0],
      [0,1,1,1,1,1,1,1,0,1,0],
      [0,1,1,1,1,0,1,1,0,1,0],
      [0,1,1,1,1,1,1,1,0,1,0],
      [0,1,1,1,1,1,1,1,1,1,0],
      [0,1,0,0,1,1,0,0,1,0,0],
      [0,1,0,1,1,1,1,1,1,0,0],
      [0,0,0,0,0,0,0,0,0,0,0]]

class Player (object):

    def __init__(self):
        self.x, self.y, self.z = 2.8, 3.1, 0.0
        self.zoom = 4.0
        self.rotz = 0.0

    def mouse_rotate(self, amt):
        self.rotz += amt / 3

    def update(self, up, down, right, left, jump):
        if up:
            self.x -= math.cos(math.radians(self.rotz)) * -1.0 * 0.023
            self.y -= math.sin(math.radians(self.rotz)) * 0.023
        if down:
            self.x += math.cos(math.radians(self.rotz)) * -1.0 * 0.023
            self.y += math.sin(math.radians(self.rotz)) * 0.023
        if right:
            self.rotz += 2.0
        if left:
            self.rotz -= 2.0
        if self.z == 0.0:
            if jump:
                self.z += 0.1
            if self.z <= 0.25:
                self.z = 0.25
        if not jump:
            self.z = 0


class Model(object):
    def __init__(self, image):
        self.image = pygame.image.load(image)
        self.display_list = glGenLists(1);
        self.build_display_list()

    def build_display_list(self):
        glNewList(self.display_list, GL_COMPILE)
        SIZE = 0.01
        WIDTH, HEIGHT = self.image.get_size()
        glTranslatef( 0.0, (-1.0 * SIZE) * (WIDTH/2.0),	SIZE * HEIGHT)
        for y in range(0, HEIGHT):
            for x in range(0, WIDTH):
                glTranslatef(0.0, SIZE, 0.0)
                if not self.image.get_at((x,y)) == (255,0,255):
                    self.draw_colored_box(SIZE, self.image.get_at((x,y)) )
            glTranslatef( 0.0, (WIDTH)* (-1.0 * SIZE), (-1.0 * SIZE))
        glEndList()

    def draw_colored_box(self, size, color):
        glColor3f(float(color[0])/255.0,float(color[1])/255.0,float(color[2])/255.0)
        glutSolidCube(size)

def init(width, height):
    pygame.init()
    glutInit([], [])
    screen = pygame.display.set_mode((width, height), OPENGL|DOUBLEBUF|RESIZABLE)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glClearColor(0.0, 0.0, 255.0, 0.0)
    glClearDepth(1.0)
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(40.0, float(width)/float(height), 0.1, 1000.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    return screen


def load_textures():
    image = Surface((velikost, velikost))
    image.convert()
    image.fill(Color("#FFFFFF"))
    ix, iy = image.get_width(), image.get_height()
    image = pygame.image.tostring(image, 'RGBX', True)

    glPixelStorei(GL_UNPACK_ALIGNMENT,1)
    glTexImage2D(GL_TEXTURE_2D, 0, 3, ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, image)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)

### definice ctverce pro mapu bylo by dobre predelat
### toto je  kde bude kostka kdyz to bude jak mc nebo to zustane takto


def draw_room(x, y):
    glBegin(GL_QUADS)
    glColor3f(1.0, 1.0, 1.0)
    glNormal3f(0.0, 0.0, 0.5)
    glTexCoord2f(0.5, 0.0)
    # definice x, y, z pro pravej dolni roh obrazku
    glVertex3f( 0.5, 0.0, 0.0)
    ##########################
    glNormal3f(0.0, 0.0, 1.0)
    glTexCoord2f(0.0, 0.0)
    # definice x, y, z pro pravej horni roh roh obrazku
    glVertex3f( 0.5, 0.5, 0.0)
    ##########################
    glNormal3f(0.0, 0.0, 1.0)
    glTexCoord2f(0.0, 0.5)
    # definice x, y, z pro levej horni roh
    glVertex3f( 0.0, 0.5, 0.0)
    ##########################
    glNormal3f(0.0, 0.0, 1.0)
    glTexCoord2f(0.5, 0.5)
    # definice x, y, z pro levej dolni roh
    glVertex3f( 0.0, 0.0, 0.0)
    ##########################
    # mapa je posazena jakoby ze zhora + otocena o 180 stupnu

    if not MAP[y][x-1]:
        glNormal3f(1.0, 0.0, 0.0)
        glTexCoord2f(1.0, 1.0)
        glVertex3f( 0.0, 0.0, 0.0)
        glNormal3f(1.0, 0.0, 0.0)
        glTexCoord2f(1.0, 0.5)
        glVertex3f( 0.0, 0.5, 0.0)
        glNormal3f(1.0, 0.0, 0.0)
        glTexCoord2f(0.5, 0.5)
        glVertex3f( 0.0, 0.5, 0.5)
        glNormal3f(1.0, 0.0, 0.0)
        glTexCoord2f(0.5, 1.0)
        glVertex3f( 0.0, 0.0, 0.5)

    # definice vysky zdi pokud neni vedle 1
    if not MAP[y][x+1]:
        glNormal3f(-1.0, 0.0, 0.0)
        glTexCoord2f(0.5, 1.0)
        glVertex3f( 0.5, 0.0, 0.5)
        glTexCoord2f(0.5, 0.5)
        glVertex3f( 0.5, 0.5, 0.5)
        glTexCoord2f(1.0, 0.5)
        glVertex3f( 0.5, 0.5, 0.0)
        glTexCoord2f(1.0, 1.0)
        glVertex3f( 0.5, 0.0, 0.0)

    # definice vysky zdi pokud neni vedle 1
    if not MAP[y+1][x]:
        glNormal3f(1.0, 0.0, 0.0)
        glTexCoord2f(1.0, 0.5)
        glVertex3f( 0.5, 0.5, 0.0)
        glTexCoord2f(0.5, 0.5)
        glVertex3f( 0.5, 0.5, 0.5)
        glTexCoord2f(0.5, 1.0)
        glVertex3f( 0.0, 0.5, 0.5)
        glTexCoord2f(1.0, 1.0)
        glVertex3f( 0.0, 0.5, 0.0)

    # definice vysky zdi pokud neni vedle 1
    if not MAP[y-1][x]:
        glNormal3f(1.0, 1.0, 0.0)
        glTexCoord2f(1.0, 1.0)
        glVertex3f( 0.0, 0.0, 0.0)
        glTexCoord2f(0.5, 1.0)
        glVertex3f( 0.0, 0.0, 0.5)
        glTexCoord2f(0.5, 0.5)
        glVertex3f( 0.5, 0.0, 0.5)
        glTexCoord2f(1.0, 0.5)
        glVertex3f( 0.5, 0.0, 0.0)
    glEnd()

### definice ctverce pro mapu bylo by dobre predelat
### toto je  kde bude kostka kdyz to bude jak mc nebo to zustane takto

def drawText( value, x,y,  windowHeight, windowWidth, step = 18 ):
    """Draw the given text at given 2D position in window
    """
    glColor3f(1.0, 1.0, 1.0)
    glMatrixMode(GL_PROJECTION);
    # For some reason the GL_PROJECTION_MATRIX is overflowing with a single push!
    # glPushMatrix()
    matrix = glGetDouble( GL_PROJECTION_MATRIX )

    glLoadIdentity();
    glOrtho(0.0, windowHeight or 32, 0.0, windowWidth or 32, -1.0, 1.0)
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i(x, y);
    lines = 0
##	import pdb
##	pdb.set_trace()
    for character in value:
        if character == '\n':
            glRasterPos2i(x, y-(lines*18))
        else:
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, ord(character));
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    # For some reason the GL_PROJECTION_MATRIX is overflowing with a single push!
    # glPopMatrix();
    glLoadMatrixd( matrix ) # should have un-decorated alias for this...

    glMatrixMode(GL_MODELVIEW);

def resize(width, height):
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width)/float(height), 0.1, 1000.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def draw(player):
    glEnable (GL_LIGHTING)
    px, py, pz, pzoom = player.x, player.y, player.z, player.zoom

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity()

    ly = (math.sin(math.radians(player.rotz))       ) * pzoom
    lx = (math.cos(math.radians(player.rotz)) * -1.0) * pzoom

    x = px + lx
    y = py + ly
    z = pz + pzoom / 2

    gluLookAt(x, y, z, px, py, pz, 0.0 , 0.0 , 1.0)

    glPushMatrix()
    glEnable(GL_TEXTURE_2D)
    for y in range(0, HEIGHT):
        for x in range(0, WIDTH):
            glTranslatef(0.5,0.0,0.0)
            if MAP[y][x]:
                draw_room(x,y)
                pass
        glTranslatef((WIDTH)*-0.5,0.5,0.0)
    glDisable(GL_TEXTURE_2D)
    glPopMatrix()



    glPushMatrix()
    glTranslatef(px, py, pz)
    glRotatef(player.rotz, 0.0, 0.0, -1.0)
    glCallList(player_model.display_list)
    glRotatef(player.rotz, 0.0, 0.0, 1.0)
    glTranslatef(0.0, 0.0, 0.8)
    glPopMatrix()

    glutSolidCube(0.1)
    glLightfv (GL_LIGHT0, GL_POSITION, [2.0, 2.0, 0.1, 1.1])
    glLightfv (GL_LIGHT0, GL_AMBIENT, [0.0, 0.0, 0.0, 1.0])
    glLightfv (GL_LIGHT0, GL_DIFFUSE, [0.0, 1.0, 1.0, 1.0])
    glLightfv (GL_LIGHT0, GL_SPECULAR, [0.5, 0.5, 0.5, 1.0])

    glDisable(GL_LIGHTING)
    string = "fps: %d x: %f y: %f z: %f" % (game_time.get_fps(), px, py, pz)
    drawText(string, 10,10, 1000, 900)
    glEnable(GL_LIGHTING)

    glTranslate(0.0, 0.0, 0.0)
    glRotate(90.0, 0.0, 0.0, -1.0)
    glCallList(other_model.display_list)

    pygame.display.flip()


screen = init(WIN_WIDTH, WIN_HEIGHT)
load_textures()
game_time = pygame.time.Clock()
player = Player()
player_model = Model("assets/char.bmp")
other_model = Model("assets/sword.bmp")
run = True
rotating = False
while run:
    pressed = pygame.key.get_pressed()
    up, down, left, right, jump = [pressed[key] for key in (K_w, K_s, K_a, K_d, K_SPACE)]
    for event in pygame.event.get():
        if event.type == pygame.QUIT: run = False

        elif event.type == MOUSEBUTTONDOWN:
            if event.button == 5:
                player.zoom += 0.1
            if event.button == 4:
                player.zoom -= 0.1
            if event.button == 3:
                rotating = True
            print event.pos, event.button

        elif event.type == MOUSEBUTTONUP:
            if event.button == 3:
                rotating = False

        elif event.type == MOUSEMOTION:
            if rotating:
                player.mouse_rotate(event.rel[0])

        elif event.type == VIDEORESIZE:
            print event
            resize(event.w, event.h)

            print game_time.get_fps()
    player.update(up, down, right, left, jump)
    draw(player)
    game_time.tick()

#####################################################################
class Scene(object):
    def __init__(self):
        pass

    def render(self, screen):
        raise NotImplementedError

    def update(self):
        raise NotImplementedError

    def handle_events(self, events):
        raise NotImplementedError


class GameScene(Scene):

    def __init__(self):
        super(GameScene, self).__init__()
        self.bg = modra
        self.entities = pygame.sprite.Group()
        self.platforms = []
        self.font = pygame.font.SysFont('Arial', 32)
        pygame.mouse.set_visible(False)
        pygame.mouse.set_pos(WIN_WIDTH / 2, WIN_HEIGHT / 2)
        self.text = "fps: %d x: %f y: %f z: %f" % (game_time.get_fps(), px, py, pz)
        self.levelno = levelno
        levelinfo = levels[levelno]
        level = levelinfo['level']
        total_level_width = len(level[0]) * velikost
        total_level_height = len(level) * velikost